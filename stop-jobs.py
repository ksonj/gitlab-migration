import itertools
import os
import sys
from dataclasses import dataclass
from typing import Any, Iterator, NewType, Optional

from requests import Response, Session

ProjectID = NewType("ProjectID", int)
PipelineID = NewType("PipelineID", int)
URL = NewType("URL", str)


@dataclass
class Pipeline:
    id: PipelineID
    project: ProjectID


@dataclass
class Credentials:
    token: str

    @staticmethod
    def from_env() -> "Credentials":
        token = os.environ.get("GITLAB_PRIVATE_TOKEN")
        if token is None:
            raise Exception("GITLAB_PRIVATE_TOKEN not set")
        return Credentials(token=token)


class KillJobs:
    GET_PIPELINES = "/projects/{projectId}/pipelines"
    CANCEL_PIPELINE = "/projects/{projectId}/pipelines/{pipelineId}/cancel"
    PAGE_SIZE = 10

    @staticmethod
    def _session(creds: Credentials) -> Session:
        s = Session()
        s.headers.update({"Private-Token": creds.token})
        return s

    @classmethod
    def load(cls, url: Optional[URL] = None) -> "KillJobs":
        creds = Credentials.from_env()
        session = cls._session(creds)
        return cls(session, url)

    def __init__(self, session: Session, url: Optional[URL] = None) -> None:
        self.session = session
        self.url = url or URL("https://gitlab.com/api/v4")

    def _get_pipelines(self, projectId: ProjectID) -> Iterator[Pipeline]:
        path = self.GET_PIPELINES.format(projectId=projectId)
        for i in itertools.count():
            page = i + 1
            resp = self.session.get(
                f"{self.url}{path}?per_page={self.PAGE_SIZE}&page={page}"
            )
            resp.raise_for_status()
            data = resp.json()
            if len(data) <= 0:
                print(f"No more data after {i} pages")
                break
            for pl in data:
                yield Pipeline(id=PipelineID(pl["id"]), project=projectId)

    def _cancel_jobs(self, pipeline: Pipeline) -> None:
        path = self.CANCEL_PIPELINE.format(
            projectId=pipeline.project, pipelineId=pipeline.id
        )
        print(f"Kill pipeline {pipeline.id}")
        resp = self.session.post(f"{self.url}{path}")
        resp.raise_for_status()

    def __call__(self, projectId: ProjectID) -> None:
        for pipeline in self._get_pipelines(projectId):
            self._cancel_jobs(pipeline)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} PROJECT_ID")
        print("Set GITLAB_PRIVATE_TOKEN to an access token with full API access")
        sys.exit(1)
    pId = sys.argv[1]
    kill_all = KillJobs.load()
    kill_all(ProjectID(int(pId)))
