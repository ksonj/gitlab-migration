{
  description = "A simple dev shell flake";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat.url = "github:edolstra/flake-compat";
    flake-compat.flake = false;
  };
  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        python = pkgs.python310.withPackages (p: [ p.requests ]);
        stop-jobs = pkgs.writeShellApplication {
          name = "stop-jobs";
          text = ''
            ${python}/bin/python ./stop-jobs.py "$@"
          '';
        };
      in
      {
        devShells.default = pkgs.mkShell {
          packages = with pkgs;[ python ];
        };
        apps = {
          stop-jobs = {
            type = "app";
            program = "${stop-jobs}/bin/stop-jobs";
          };
        };
      });
}
